-= Justified Drupal module =-

Summary
=========================
This module provides integration with Justified jquery plugin.
Justified jquery plugin allows you to create Justified Image Gallery

Requirements
=========================
 * Justified jquery plugin should be installed to sites/all/libraries/justified directory.

Installation
=========================
 * Install as usual, see http://drupal.org/node/895232 for further information.
 * Download Justified library from https://github.com/nitinhayaran/Justified.js
 * Unzip the library to sites/all/libraries/justified directory.

Notes
=========================
 * You can use `drush justified-download` command for easy installation of the library.
 * For security reasons, it is recommended that you remove 'demo' directory from the library.

Roadmap
=========================
 * Add integration with colorbox module.